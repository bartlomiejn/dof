//
//  WeakSelf.h
//  DoFRendering
//
//  Created by Bartłomiej Nowak on 08/11/2018.
//  Copyright © 2018 Bartłomiej Nowak. All rights reserved.
//

#ifndef WeakSelf_h
#define WeakSelf_h

#define WEAK_SELF __weak __typeof__(self)

#endif /* WeakSelf_h */
