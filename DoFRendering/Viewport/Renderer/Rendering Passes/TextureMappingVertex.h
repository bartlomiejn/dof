//
//  TextureMappingVertex.h
//  DoFRendering
//
//  Created by Bartłomiej Nowak on 14/11/2018.
//  Copyright © 2018 Bartłomiej Nowak. All rights reserved.
//

#ifndef TextureMappingVertex_h
#define TextureMappingVertex_h

#import <simd/simd.h>

typedef struct {
    simd::float4 renderedCoordinate [[position]];
    simd::float2 textureCoordinate;
} TextureMappingVertex;

#endif /* TextureMappingVertex_h */
