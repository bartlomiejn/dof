//
//  CircleOfConfusion.metal
//  DoFRendering
//
//  Created by Bartłomiej Nowak on 14/11/2018.
//  Copyright © 2018 Bartłomiej Nowak. All rights reserved.
//

#include <metal_stdlib>
#include "TextureMappingVertex"

using namespace metal;

typedef struct {
    float focusDist, focusRange;
} CoCUniforms;

constexpr sampler texSampler(address::clamp_to_zero, filter::linear, coord::normalized);

fragment half4
circle_of_confusion_pass(TextureMappingVertex vert [[stage_in]],
                         constant CoCUniforms *uni [[buffer(0)]],
                         depth2d<float, access::sample> depthTex [[texture(0)]])
{
    half depth = depthTex.sample(texSampler, vert.textureCoordinate);
    float coc = (depth - uni->focusDist) / uni->focusRange;
    return half4(coc);
}

