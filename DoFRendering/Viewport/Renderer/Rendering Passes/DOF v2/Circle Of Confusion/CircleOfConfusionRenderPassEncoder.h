//
//  CircleOfConfusionRenderPassEncoder.h
//  DoFRendering
//
//  Created by Bartłomiej Nowak on 14/11/2018.
//  Copyright © 2018 Bartłomiej Nowak. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CircleOfConfusionRenderPassEncoder : NSObject

@end

NS_ASSUME_NONNULL_END
